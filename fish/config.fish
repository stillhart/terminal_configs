# CHRUBY
if test -f "/usr/share/chruby/chruby.fish"
    set -gx CHRUBY_ROOT /usr
    source /usr/share/chruby/chruby.fish
    echo usr
end
if test -f "/usr/local/share/chruby/chruby.fish"
    set -g -x CHRUBY_ROOT /usr/local/
    source /usr/local/share/chruby/chruby.fish
    echo usr
end


if test -f "/usr/share/chruby/auto.fish"
    set -gx CHRUBY_ROOT /usr
    source /usr/share/chruby/auto.fish
    echo local
end
if test -f "/usr/local/share/chruby/auto.fish"
    set -g -x CHRUBY_ROOT /usr/local/
    source /usr/local/share/chruby/auto.fish
    echo local
end

# Apple M1 architecture , v1 ?
# macOS check
if test (uname -s) = Darwin
    # brew install chruby-fish
    source /opt/homebrew/share/fish/vendor_functions.d/*
    # Apple M1 architecture
    if test -f "/opt/homebrew/share/fish/vendor_functions.d/chruby.fish"
    else
        echo "my chruby loader - brew install chruby-fish did not happen or changed"
    end
    #if test -f "/opt/homebrew/share/chruby/chruby.fish"
    #    set -gx CHRUBY_ROOT /opt/homebrew
    #    source /opt/homebrew/share/chruby/chruby.fish
    #    echo m1
    #end
    #if test -f "/opt/homebrew/share/chruby/auto.fish"
    #    set -gx CHRUBY_ROOT /opt/homebrew
    #    source /opt/homebrew/share/chruby/auto.fish
    #    echo m1
    #end
else if test (uname -s) = Linux
    set os_release (cat /etc/os-release | grep VERSION_ID | cut -d '=' -f 2 | tr -d '"')
    # RHEL7 check
    if string match -r "7\..*" "$os_release"
        if test -f ~/.config/terminal_configs/fish/chruby.fish
            set -gx CHRUBY_ROOT /usr
            source ~/.config/terminal_configs/fish/chruby.fish
            echo rhel
        end
        if test -f ~/.config/terminal_configs/fish/chruby-auto.fish
            set -gx CHRUBY_ROOT /usr
            source ~/.config/terminal_configs/fish/chruby-auto.fish
            echo rhel
        end
    else if string match -r "9\..*" "$os_release"
        if test -f ~/.config/terminal_configs/fish/chruby.fish
            set -gx CHRUBY_ROOT /usr
            source ~/.config/terminal_configs/fish/chruby.fish
            echo rhel
        end
        if test -f ~/.config/terminal_configs/fish/chruby-auto.fish
            set -gx CHRUBY_ROOT /usr
            source ~/.config/terminal_configs/fish/chruby-auto.fish
            echo rhel
        end
    else
        echo "my chruby loader - Unsupported Linux OS version"
    end
else
    echo "my chruby loader - Unsupported Linux OS version"
end

set -x EDITOR vim
set -x GIT_EDITOR vim
set -x REPORTTIME 2
set -x GIT_PAGER ""
set -x TERM xterm-256color

set fish_greeting ""

set -g -x LANG "en_US.UTF-8"
set -g -x LC_CTYPE UTF-8
set -g -x LC_NUMERIC "en_US.UTF-8"
set -g -x LC_TIME "en_US.UTF-8"
set -g -x LC_COLLATE "en_US.UTF-8"
set -g -x LC_MONETARY "en_US.UTF-8"
set -g -x LC_MESSAGES "en_US.UTF-8"
set -g -x LC_PAPER "en_US.UTF-8"
set -g -x LC_NAME "en_US.UTF-8"
set -g -x LC_ADDRESS "en_US.UTF-8"
set -g -x LC_TELEPHONE "en_US.UTF-8"
set -g -x LC_MEASUREMENT "en_US.UTF-8"
set -g -x LC_IDENTIFICATION "en_US.UTF-8"
set -g -x LC_ALL "en_US.UTF-8"

if test -d /opt/homebrew/bin
    set -U fish_user_paths /opt/homebrew/bin $fish_user_paths
end

. ~/.config/fish/config_local.fish
. ~/.config/fish/aliases.fish
. ~/.config/fish/aliases_local.fish
. ~/.config/terminal_configs/fish/functions/clean_docker.fish
