# MY terminal configs

This repo is super duper extremly opinionated. 

# get started

```
cd ~
mkdir ~/.config
cd ~/.config
git clone https://gitlab.com/stillhart/terminal_configs.git
cd ~/.config/terminal_configs
```

## zsh
```
cd ~
touch ~/.zshrc_local
ln -s ~/.config/terminal_configs/.zshrc .zshrc
```

## bash
```
cp ~/.config/terminal_configs/.bashrc_local ~/.bashrc
```

## tmux
```
cd ~
ln -s ~/.config/terminal_configs/.tmux.conf .tmux.conf
```

## fish

```
cd ~
mkdir -p ~/.config/fish
touch ~/.config/fish/config_local.fish
touch ~/.config/fish/aliases_local.fish

cd ~/.config/fish
ln -s ../terminal_configs/fish/config.fish  config.fish
ln -s ../terminal_configs/fish/aliases.fish aliases.fish

cd ~
mkdir -p ~/.config/fish/functions
cd ~/.config/fish/functions
ln -s ../../terminal_configs/fish/functions/be.fish       be.fish
ln -s ../../terminal_configs/fish/functions/beirb.fish    beirb.fish
ln -s ../../terminal_configs/fish/functions/berc.fish     berc.fish
ln -s ../../terminal_configs/fish/functions/fish_prompt.fish fish_prompt.fish
ln -s ../../terminal_configs/fish/functions/fish_right_prompt.fish fish_right_prompt.fish
ln -s ../../terminal_configs/fish/functions/foricons.fish foricons.fish 
ln -s ../../terminal_configs/fish/functions/foriman.fish foriman.fish
ln -s ../../terminal_configs/fish/functions/ga.fish ga.fish 
ln -s ../../terminal_configs/fish/functions/gc.fish gc.fish 
ln -s ../../terminal_configs/fish/functions/guardian.fish guardian.fish 
ln -s ../../terminal_configs/fish/functions/mux0.fish mux0.fish
ln -s ../../terminal_configs/fish/functions/mux.fish mux.fish
ln -s ../../terminal_configs/fish/functions/tmux0.fish tmux0.fish 
ln -s ../../terminal_configs/fish/functions/tmux1.fish tmux1.fish
ln -s ../../terminal_configs/fish/functions/vi.fish vi.fish
ln -s ../../terminal_configs/fish/functions/audit.fish audit.fish
```


## VIM
```
cd ~
ln -s ~/.config/terminal_configs/.vimrc .vimrc
```

## irb history
```
cd ~
ln -s ~/.config/terminal_configs/.irbrc .irbrc
```

## git
```
cd ~
ln -s ~/.config/terminal_configs/.gitconfig .gitconfig
```



## ruby
```sh
sudo apt-get install ruby
```
[ruby-install](https://github.com/postmodern/ruby-install#install)

[chruby](https://github.com/postmodern/chruby#install)


### install latest and greatest ruby
```sh
cd ~
ruby-install ruby -L --no-reinstall
```

### NO DOC
```sh
cat <<EOF > ~/.gemrc
EOF
```

### default Gemfile for home
```sh
cd ~
mv ~/Gemfile Gemfile_REPLACED
ln -s ~/.config/terminal_configs/Gemfile Gemfile
```
```sh
cd ~
gem install bundler
bundle install
```

### .ruby-version
```sh
cat <<EOF > ~/.ruby-version
3.4
EOF
```

## crontab
```
6 9 */10 * * bash -l -c "cd ~/.config/terminal_configs; git fetch" >/dev/null 2>&1
1 9 */10 * * bash -l -c "cd ~; gem update --system" >/dev/null 2>&1
2 9 */10 * * bash -l -c "cd ~; gem update bundler" >/dev/null 2>&1
3 9 */10 * * bash -l -c "cd ~; bundle update" >/dev/null 2>&1
0 7 */10 * * bash -l -c "cd ~; ruby-install ruby -L --no-reinstall" >/dev/null 2>&1
```

## Keeweb
https://keeweb.info/


# Debian

```
sudo apt install git curl wget btop htop iotop git fish zsh bash tmux vim rsync
```

```
sudo apt install ruby chruby ruby-install libsqlite3-dev sqlite3 build-essential
```

https://docs.docker.com/engine/install/debian/

# Ubuntu
Fuck Ubuntu

## ruby
```
sudo apt install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev
```

## feel like at home
```sh
sudo apt install tmux vim postgresql  sqlite git fish zsh bash curl wget htop openssl youtube-dl coreutils telnet rsync postgresql-client
```


# MAC

## homebrew
https://brew.sh/
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

## mac install
```sh
brew install tmux vim redis postgres ruby-install chruby fish sqlite git zsh bash curl wget htop openssl coreutils telnet rsync nmap
```
http://osxdaily.com/2012/03/21/change-shell-mac-os-x/

## mac apps
```sh
 brew cask install iterm2 wireshark gimp keeweb pgadmin4 postgres caffeine docker visual-studio-code vlc
```

## mac config
```sh
# search for updates every day
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1
# Expand print dialog by default (revert by changing TRUE to FALSE)
defaults write -g PMPrintingExpandedStateForPrint -bool TRUE
```