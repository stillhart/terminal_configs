alias tmux0='tmux a -t 0'
alias tmux1='tmux a -t 1'
alias ll='ls -lh'
alias la='ls -lAhF'
alias lr='la -rt'
alias vi='vim'
alias ..='cd ..'
alias -- -='cd -'

#git aliases
alias gs="git status"
alias gc="git commit -m"
alias ga="git add -p"
alias gti="git"
alias tgi="git"
alias tig="git"

#rails aliases
alias be='bundle exec'
alias bundl='bundle'
alias budle='bundle'
alias budnel='bundle'
alias bundle='bundle'


alias berr='bundle exec rails runner'
alias berc='bundle exec rails console'
alias bers='bundle exec rails server'
alias beru='bundle exec ruby'
alias beirb='bundle exec irb'
alias bersp='bundle exec rake spec'

alias generate_coverage='RAILS_ENV=test bundle exec foreman run rake spec:coverage'
alias generate_best_practices='be rails_best_practices -f html --output-file public/coverage/best_practices.html .'
alias generate_rubycritics='be rubycritic -p public/coverage/rubycritic/ app'
alias generate_rubocop='be rubocop --rails --format html -o public/coverage/rubocop.html app'
alias generate_brakeman='be brakeman -o public/coverage/brakeman.html'

alias fori='be foreman run'
alias foreman_dev='be foreman start -f Procfile-dev'
alias foriman='be foreman start'
alias foricons='be foreman run rails console'
alias guardian='be foreman run guard'

export EDITOR="vim"
export GIT_EDITOR="vim"

# https://github.com/jaivardhankapoor/bestbash/blob/master/settings
shopt -s cdspell                 # Correct cd typos
shopt -s checkwinsize            # Update windows size on command
shopt -s histappend              # Append History instead of overwriting file
shopt -s cmdhist                 # Bash attempts to save all lines of a multiple-line command in the same history entry
shopt -s expand_aliases
shopt -s extglob                 # Extended pattern
shopt -s no_empty_cmd_completion # No empty completion

# prompt
#export PS1="\u@\h:\W \[$(tput sgr0)\]"
export PS1="\[\e[32m\]\u@\h\[\e[m\]:\W "

# locales
export LANG="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_NUMERIC="de_CH.UTF-8"
export LC_TIME="de_CH.UTF-8"
export LC_COLLATE="de_CH.UTF-8"
export LC_MONETARY="de_CH.UTF-8"
export LC_MESSAGES="en_US.UTF-8" 

function clean_docker(){
	docker kill $(docker ps -q);
	docker rm $(docker ps -a -q);
	docker rmi $(docker images -q);
  docker system prune -f;
}
